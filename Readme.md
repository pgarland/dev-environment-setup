# Dev Environment Setup (for MacOS)
***
## Tutorials
- http://vim-adventures.com/
- https://www.bloc.io/ruby-warrior/#/
- http://www.learnpython.org/
- go through the vim tutorial (maybe skip if you completed vim-adventures):
```
#!bash
brew install vim
vimtutor
```
  note: scroll down using 'j', up using 'k'
- learn git (very important):
    - https://try.github.io/levels/1/challenges/1
    - http://gitimmersion.com/
    - https://github.com/Gazler/githug
- program a tank in ruby and compete against other people - https://github.com/awilliams/RTanque

## Recommended Reading
- http://www.ignoredbydinosaurs.com/2014/05/what-is-my-path
- https://zapier.com/learn/apis/
- http://askubuntu.com/questions/506510/what-is-the-difference-between-terminal-console-shell-and-command-line
- learn what a package manager is: https://en.wikipedia.org/wiki/Package_manager
- learn about a specific package manager youll be using:
    - https://en.wikipedia.org/wiki/Python_Package_Manager
    - https://en.wikipedia.org/wiki/RubyGems
- look over these useful command line snippets:
```
#!bash
cd my/directory/path # navigate into your directory
ls -al # list all files in directory
ls -al | grep search_string # the pipe '|' in command line passes output of one command into another. this command will list files in directory, and filter file names for those containing search_string
echo foobar > my_file.txt # write 'foobar' into my_file.txt, OVERWRITING contents
echo foobar >> my_file.txt # write 'foobar' into my_file.txt, APPENDING contents
```

## Useful Links
- regex: http://www.regexr.com/
- vim commands cheatsheet: http://www.fprintf.net/vimCheatSheet.html
- https://www.checkio.org/profile/login/


## Recommended Packages
- http://brew.sh/
- https://kapeli.com/dash
- https://github.com/sstephenson/rbenv
- http://skim-app.sourceforge.net/
- https://www.join.me/
- http://www.sublimetext.com/3 (package manager: https://packagecontrol.io/installation)

_Install from Homebrew:_

- git
- node
- python
- ruby-build (https://github.com/sstephenson/ruby-build)
- rbenv-gem-rehash (https://github.com/sstephenson/rbenv-gem-rehash)
- vim

_optional:_

- http://postgresapp.com/
- tree
- tig (https://github.com/clvv/fasd)
- fasd (https://github.com/clvv/fasd)
- https://github.com/sorin-ionescu/prezto
- http://qsapp.com/
- http://www.scootersoftware.com/download.php
- sourcetree or github app
- virtualbox (https://www.virtualbox.org/wiki/Downloads)
asdf
